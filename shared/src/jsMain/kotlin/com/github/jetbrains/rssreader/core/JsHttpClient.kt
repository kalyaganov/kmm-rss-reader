/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package com.github.jetbrains.rssreader.core

import io.ktor.client.*
import io.ktor.client.engine.js.*

internal fun JsHttpClient() = HttpClient(Js) {}
