/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package com.github.jetbrains.rssreader.core

import com.github.jetbrains.rssreader.core.datasource.network.FeedParser
import com.github.jetbrains.rssreader.core.entity.Feed
import com.github.jetbrains.rssreader.core.entity.Post
import org.w3c.dom.*
import org.w3c.dom.parsing.DOMParser
import kotlin.js.Date

internal class JsFeedParser : FeedParser {
    override suspend fun parse(sourceUrl: String, xml: String, isDefault: Boolean): Feed {
        val parser = DOMParser()
        val doc = parser.parseFromString(xml, "application/xml")
        return readFeed(
            sourceUrl = sourceUrl,
            element = doc.documentElement,
            isDefault = isDefault
        )
    }

    private fun readFeed(sourceUrl: String, element: Element?, isDefault: Boolean): Feed {
        val title: String? = element
            ?.getElementsByTagName("title")
            ?.get(0)
            ?.innerHTML

        val link: String? = element
            ?.getElementsByTagName("link")
            ?.get(0)
            ?.innerHTML

        val description: String? = element
            ?.getElementsByTagName("description")
            ?.get(0)
            ?.innerHTML

        val imageUrl: String? = element
            ?.getElementsByTagName("image")
            ?.get(0)
            ?.getElementsByTagName("url")
            ?.get(0)
            ?.innerHTML

        val posts = element?.getElementsByTagName("item")?.asList()
            ?.map { readPost(title!!, it) }
            ?: emptyList()

        return Feed(
            title = title!!,
            link = link!!,
            desc = description!!,
            imageUrl = imageUrl,
            posts = posts,
            sourceUrl = sourceUrl,
            isDefault = isDefault
        )
    }

    private fun readPost(feedTitle: String, element: Element): Post {
        val title: String? = element
            .getElementsByTagName("title")[0]
            ?.innerHTML

        val link: String? = element
            .getElementsByTagName("link")[0]
            ?.innerHTML

        val description: String? = element
            .getElementsByTagName("description")[0]
            ?.innerHTML

        val date: String? = element
            .getElementsByTagName("pubDate")[0]
            ?.innerHTML

        val content: String? = element
            .getElementsByTagName("content:encoded")[0]
            ?.innerHTML

        return Post(
            title = title ?: feedTitle,
            link = FeedParser.cleanText(link),
            desc = FeedParser.cleanTextCompact(description),
            imageUrl = FeedParser.pullPostImageUrl(link, description, content),
            date = (Date(date!!).getTime() / 1000).toLong()
        )
    }
}
