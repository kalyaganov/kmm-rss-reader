/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package com.github.jetbrains.rssreader.core

import com.github.jetbrains.rssreader.core.datasource.network.FeedLoader
import com.github.jetbrains.rssreader.core.datasource.storage.FeedStorage
import com.russhwolf.settings.StorageSettings
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.promise
import kotlinx.serialization.json.Json

@JsExport
@OptIn(DelicateCoroutinesApi::class, ExperimentalJsExport::class)
@Suppress("unused", "NON_EXPORTABLE_TYPE")
class JsRssReader : RssReader(
    FeedLoader(
        JsHttpClient(),
        JsFeedParser()
    ),
    FeedStorage(
        StorageSettings(),
        Json {
            ignoreUnknownKeys = true
            isLenient = true
            encodeDefaults = false
        }
    )
) {
    fun getAllFeedsPromise(
        forceUpdate: Boolean = false
    ) = GlobalScope.promise {
        getAllFeeds(forceUpdate)
    }

    fun addFeedPromise(
        url: String
    ) = GlobalScope.promise {
        addFeed(url)
    }

    fun deleteFeedPromise(
        url: String
    ) = GlobalScope.promise {
        deleteFeed(url)
    }

    fun loadFeedPromise(
        url: String
    ) = GlobalScope.promise {
        loadFeed(url)
    }
}
