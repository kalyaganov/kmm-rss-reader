/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package com.github.jetbrains.rssreader.core.datasource.network

import com.github.jetbrains.rssreader.core.entity.Feed
import io.ktor.http.*

interface FeedParser {
    suspend fun parse(sourceUrl: String, xml: String, isDefault: Boolean): Feed

    companion object {
        private val imgReg = Regex("<img[^>]+\\bsrc=[\"']([^\"']+)[\"']")
        private val htmlTag = Regex("""(<!\[CDATA\[)?(.*)?(\]\])?>""")
        private val blankLine = Regex("""^(\s|\t)+|(\s|\t)+$""")

        private fun findImageUrl(ownerLink: String, text: String): String? =
            imgReg.find(text)?.value?.let { v ->
                val i = v.indexOf("src=") + 5 //after src="
                val url = v.substring(i, v.length - 1)
                if (url.startsWith("http")) url else {
                    URLBuilder(ownerLink).apply {
                        encodedPath = url
                    }.buildString()
                }
            }

        internal fun cleanText(text: String?): String? =
            text?.replace(htmlTag, "$2")
                ?.replace(blankLine, "")
                ?.trim()

        internal fun cleanTextCompact(text: String?) = cleanText(text)?.take(300)

        internal fun pullPostImageUrl(postLink: String?, description: String?, content: String?): String? =
            postLink?.let { l ->
                description?.let { findImageUrl(l, it) }
                    ?: content?.let { findImageUrl(l, it) }
            }
    }
}
