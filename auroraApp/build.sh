#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

## To KMM project
cd ../

## Build KMM JS
./gradlew packJsPackage

## To JS project
cd auroraApp/wrapper || exit

## https://github.com/webpack/webpack/issues/14532
## for mode: 'production'
## export NODE_OPTIONS=--openssl-legacy-provider

## Update dependencies
npm update

## Build JS Wrapper
npm run build
