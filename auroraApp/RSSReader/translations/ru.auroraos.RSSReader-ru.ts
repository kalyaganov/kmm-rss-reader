<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>BottomBarItemAll</name>
    <message>
        <location filename="../qml/pages/BottomBarItemAll.qml" line="30"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="95"/>
        <source>KMM RSS Reader</source>
        <translation>КММ RSS-Ридер</translation>
    </message>
</context>
<context>
    <name>DelegateFeed</name>
    <message>
        <location filename="../qml/pages/DelegateFeed.qml" line="13"/>
        <source>Removing a feed</source>
        <translation>Удаление ленты</translation>
    </message>
    <message>
        <location filename="../qml/pages/DelegateFeed.qml" line="25"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>DialogFeedAdd</name>
    <message>
        <location filename="../qml/pages/DialogFeedAdd.qml" line="31"/>
        <source>New feed</source>
        <translation>Новая лента</translation>
    </message>
    <message>
        <location filename="../qml/pages/DialogFeedAdd.qml" line="32"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../qml/pages/DialogFeedAdd.qml" line="39"/>
        <source>Feed URL</source>
        <translation>URL</translation>
    </message>
</context>
<context>
    <name>FeedActionDelete</name>
    <message>
        <location filename="../qml/pages/FeedActionDelete.qml" line="33"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../qml/pages/FeedActionDelete.qml" line="64"/>
        <source>Yes, delete</source>
        <translation>Да, удалить</translation>
    </message>
</context>
<context>
    <name>PageFeeds</name>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="32"/>
        <source>Error update feed</source>
        <translation>Ошибка обновления</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="47"/>
        <source>Feed successfully added</source>
        <translation>Успешно добавлено</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="54"/>
        <source>Error adding feed</source>
        <translation>Ошибка при добавлении</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="69"/>
        <source>Feed deleted successfully</source>
        <translation>Успешно удалено</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="76"/>
        <source>Error while deleting feed</source>
        <translation>Ошибка при удалении</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="118"/>
        <source>Long press will bring up the context menu.</source>
        <translation>Длительное нажатие вызовет контекстное меню</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="122"/>
        <location filename="../qml/pages/PageFeeds.qml" line="169"/>
        <source>Feeds</source>
        <translation>Лента</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="132"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageFeeds.qml" line="138"/>
        <source>Reload</source>
        <translation>Обновить</translation>
    </message>
</context>
<context>
    <name>PageMain</name>
    <message>
        <location filename="../qml/pages/PageMain.qml" line="35"/>
        <source>Error update feed</source>
        <translation>Ошибка обновления</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageMain.qml" line="106"/>
        <location filename="../qml/pages/PageMain.qml" line="155"/>
        <source>Posts</source>
        <translation>Новости</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageMain.qml" line="116"/>
        <source>Edit feeds</source>
        <translation>Редактировать ленты</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageMain.qml" line="124"/>
        <source>Reload</source>
        <translation>Обновить</translation>
    </message>
</context>
</TS>
