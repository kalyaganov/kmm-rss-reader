/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0
import "../pages"

CoverBackground {
    id: root

    property int index: 0
    property var model

    function update(isClear) {
        // update index
        if (isClear) {
            index = 0
        } else {
            index++
        }
        // run query
        agent.run(
            "shared.Service.get.getAllFeeds(false)",
            function(response) {
                var count = 0
                for (var i = 0; i < response.length; i++) {
                    for (var j = 0; j < response[i]['posts'].length; j++) {
                        if (index === count) {
                            model = response[i]['posts'][j]
                            return;
                        }
                        count++;
                    }
                }
                // clear index
                index = 0
                // set first item
                model = count > 0 ? response[0]['posts'][0] : undefined
            },
            function(error) {
                console.error(error)
            }
        )
    }

    onStatusChanged: {
        if (status === PageStatus.Active) {
            root.update(true)
        }
    }

    Column {
        anchors {
            fill: parent
            margins: Theme.paddingLarge
        }
        visible: model !== undefined
        spacing: Theme.paddingMedium

        Text {
            width: parent.width
            maximumLineCount: 2
            text: model === undefined ? "" : model.title
            wrapMode: Text.WordWrap
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            elide: TruncationMode.Elide
            font.pixelSize: Theme.fontSizeLarge
        }

        Text {
            width: parent.width
            maximumLineCount: 5
            text: model === undefined ? "" : model.desc
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            elide: TruncationMode.Elide
            font.pixelSize: Theme.fontSizeMedium
        }
    }

    CoverActionList {
        enabled : model !== undefined
        CoverAction {
            iconSource: "image://theme/icon-m-right"
            onTriggered: root.update()
        }
    }

    CoverPlaceholder {
        visible: model === undefined
        text: qsTr("KMM RSS Reader")
        icon {
            source: Qt.resolvedUrl("../icons/ru.auroraos.RSSReader.png")
            sourceSize { width: icon.width; height: icon.height }
        }
        forceFit: true
    }
}
