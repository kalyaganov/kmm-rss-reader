/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

ListItem {
    id: root

    enabled: model.link.length !== 0
    width: parent.width
    contentHeight: content.height + Theme.paddingMedium * 2

    Column {
        id: content

        anchors.centerIn: parent
        width: parent.width - Theme.paddingLarge * 2
        spacing: Theme.paddingMedium

        Text {
            width: parent.width
            text: model.title
            wrapMode: Text.WordWrap
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            font.pixelSize: Theme.fontSizeLarge
            font.weight: Font.Medium
        }

        Item {
            width: parent.width
            height: Theme.itemSizeExtraLarge
            visible: model.imageUrl !== undefined && model.imageUrl.length !== 0 && itemImage.status !== Image.Error

            BusyIndicator {
                anchors.centerIn: parent
                size: BusyIndicatorSize.Small
                visible: itemImage.status == Image.Loading
                running: itemImage.status == Image.Loading
            }

            Image {
                id: itemImage

                anchors.fill: parent
                source: Qt.resolvedUrl(model.imageUrl)
                fillMode: Image.PreserveAspectCrop
            }

            ColorOverlay {
                anchors.fill: itemImage
                source: itemImage
                opacity: root.highlighted ? Theme.opacityHigh : 0.0
                color: root.highlighted ? Theme.highlightColor : "transparent"
            }
        }

        Text {
            width: parent.width
            text: model.desc
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            font.pixelSize: Theme.fontSizeSmall
        }

        Separator {
            width: parent.width
            color: Theme.primaryColor
        }

        Text {
            width: parent.width
            text: Qt.formatDateTime(new Date(model.date * 1000), "dd MMMM yyyy")
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            font.pixelSize: Theme.fontSizeExtraSmall
        }
    }
}
