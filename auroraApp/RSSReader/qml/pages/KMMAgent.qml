/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.WebView 1.0
import Sailfish.WebEngine 1.0

Item {
    id: root

    property var stateResponse: ({})

    signal completed()

    function run(method, result, error) {
        if (method.indexOf("return") === -1) {
            webview.runJavaScript("return " + method, function(key) {
                root.stateResponse[key] = [result, error]
            }, error);
        } else {
            webview.runJavaScript(method, result, error);
        }
    }

    WebView {
        id: webview

        height: 0
        width: 0
        url: Qt.resolvedUrl("../shared-js/index.html")
        visible: false

        onViewInitialized: {
            webview.loadFrameScript(Qt.resolvedUrl("../shared-js/framescript.js"));
            webview.addMessageListener("webview:action")
        }

        onRecvAsyncMessage: {
            switch (message) {
            case "webview:action":
                try {
                    if (data.caller === 'init') {
                        root.completed()
                    } else if (root.stateResponse[data.caller] !== undefined && root.stateResponse[data.caller].length === 2) {
                        if (data.response.hasOwnProperty('stack')) {
                            // error
                            root.stateResponse[data.caller][1](data.response.message)
                        } else {
                            // success
                            root.stateResponse[data.caller][0](data.response)
                        }
                    }
                } catch (e) {
                    root.stateResponse[data.caller][1](e.toString())
                    console.log(e)
                }
                break
            }
        }

        Component.onCompleted: {
            WebEngineSettings.setPreference("security.disable_cors_checks", true, WebEngineSettings.BoolPref)
        }
    }
}

