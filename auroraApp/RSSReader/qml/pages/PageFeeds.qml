/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: root

    property bool loading
    property var setIsUpdate
    property var feeds: []

    function update(forceUpdate) {
        // start loading
        loading = true

        // run query
        agent.run(
            "shared.Service.get.getAllFeeds(" + forceUpdate + ")",
            function(response) {
                // save response
                feeds = response

                // update list models
                updateLists(feeds)

                // disable loading
                loading = false
            },
            function(error) {
                toastShow(qsTr("Error update feed"))
                // disable loading
                loading = false
            }
        )
    }

    function addFeed(url) {
        // start loading
        loading = true

        // run query
        agent.run(
            "shared.Service.get.addFeed('" + url + "')",
            function(response) {
                toastShow(qsTr("Feed successfully added"))
                // update from cache
                update(false)
                // set main page is update
                setIsUpdate()
            },
            function(error) {
                toastShow(qsTr("Error adding feed"))
                // disable loading
                loading = false
            }
        )
    }

    function deleteFeed(url) {
        // start loading
        loading = true

        // run query
        agent.run(
            "shared.Service.get.deleteFeed('" + url + "')",
            function(response) {
                toastShow(qsTr("Feed deleted successfully"))
                // update from cache
                update(false)
                // set main page is update
                setIsUpdate()
            },
            function(error) {
                toastShow(qsTr("Error while deleting feed"))
                // disable loading
                loading = false
            }
        )
    }

    function updateLists(feeds) {
        // clear lists
        listModelFeed.clear()

        // update list models
        for (var i = 0; i < feeds.length; i++) {
            // add to list model
            listModelFeed.append(feeds[i])
        }

        // scroll to top
        listPosts.scrollToTop()
    }

    allowedOrientations: Orientation.All

    ListModel {
        id: listModelFeed
    }

    DialogFeedAdd {
        id: dialogFeedAdd

        onSubmit: addFeed(url)
    }

    SilicaListView {
        id: listPosts

        anchors.fill: parent
        visible: !loading
        model: listModelFeed
        delegate: DelegateFeed {
            onDeleteItem: deleteFeed(url)
            onClicked: {
                toastShow(qsTr("Long press will bring up the context menu."))
            }
        }
        header: PageHeader {
            title: qsTr("Feeds")
        }

        PullDownMenu {
            id: menu

            topMargin: Theme.paddingLarge
            bottomMargin: Theme.paddingLarge

            MenuItem {
                text: qsTr("Add")

                onClicked: dialogFeedAdd.open()
            }

            MenuItem {
                text: qsTr("Reload")

                onClicked: root.update(true)
            }
        }

         VerticalScrollDecorator {}
    }

    SilicaListView {
        id: listSkeleton

        anchors {
            fill: parent
            bottomMargin: Theme.horizontalPageMargin
        }

        visible: loading
        spacing: Theme.paddingLarge
        interactive: false
        model: ListModel {
            ListElement { isTime: false }
            ListElement { isTime: false }
            ListElement { isTime: false }
            ListElement { isTime: false }
            ListElement { isTime: false }
            ListElement { isTime: false }
            ListElement { isTime: false }
        }
        delegate: DelegateSkeleton {}
        header: PageHeader {
            title: qsTr("Feeds")
        }

        PullDownMenu {
            busy: true
        }
    }

    Component.onCompleted: {
        updateLists(feeds)
    }
}
