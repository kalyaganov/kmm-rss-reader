/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    id: root

    property alias icon: icon

    signal clicked

    width: parent.height
    height: parent.height
    radius: parent.height

    BackgroundItem {
        anchors.fill: parent
        contentItem.radius: parent.height

        onClicked: root.clicked()

        Icon {
            id: icon

            anchors.centerIn: parent
            width: Theme.itemSizeMedium / 2
            height: Theme.itemSizeMedium / 2
            color: highlighted ? Theme.highlightColor : "black"
        }
    }
}
