/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

Item {
    id: root

    property int duration: 1500

    readonly property int _heightTitle: Theme.fontSizeLarge * 2
    readonly property int _heightContent: Theme.fontSizeSmall * 8
    readonly property int _heightDate: Theme.fontSizeExtraSmall
    readonly property int _widthDate: Theme.buttonWidthSmall

    width: parent.width
    height: content.implicitHeight + Theme.paddingLarge * 2

    Rectangle {
        anchors.fill: parent
        color: Theme._coverOverlayColor
    }

    Column {
        id: content

        anchors.centerIn: parent
        width: parent.width - Theme.paddingLarge * 2
        spacing: Theme.paddingMedium
        opacity: Theme.opacityFaint

        Behavior on opacity {
            NumberAnimation {
                id: animation

                properties: "opacity";
                easing.type: Easing.InOutQuad;
                duration: root.duration

                onRunningChanged: {
                    if (!animation.running) {
                        content.opacity = content.opacity == Theme.opacityLow ? Theme.opacityFaint : Theme.opacityLow
                    }
                }
            }
        }

        Rectangle {
            width: parent.width
            height: _heightTitle
            color: Theme.primaryColor
            radius: Theme.paddingSmall
        }

        Rectangle {
            width: parent.width
            height: _heightContent
            color: Theme.primaryColor
            radius: Theme.paddingSmall
        }

        Separator {
            width: parent.width
            visible: isTime
            color: Theme.primaryColor
        }

        Rectangle {
            width: _widthDate
            height: _heightDate
            visible: isTime
            color: Theme.primaryColor
            radius: Theme.paddingSmall
        }

        Component.onCompleted: {
            content.opacity = Theme.opacityLow
        }
    }
}
