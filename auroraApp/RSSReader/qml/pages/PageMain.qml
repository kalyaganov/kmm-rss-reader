/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: root

    property string currentRSS: ""
    property bool isUpdate
    property bool loading: true
    property var feeds: []

    function update(forceUpdate) {
        // disable update page
        isUpdate = false
        // start loading
        loading = true

        // run query
        agent.run(
            "shared.Service.get.getAllFeeds(" + forceUpdate + ")",
            function(response) {
                // save response
                feeds = response

                // update list models
                updateLists("", response)

                // disable loading
                loading = false
            },
            function(error) {
                toastShow(qsTr("Error update feed"))
                // disable loading
                loading = false
            }
        )
    }

    function updateLists(currentUrl, feeds) {
        // update current RSS URL
        root.currentRSS = currentUrl

        // clear lists
        listModelFeed.clear()
        listModelPost.clear()

        // update list models
        for (var i = 0; i < feeds.length; i++) {
            // add to list model
            listModelFeed.append(feeds[i])

            // add posts if current
            if (currentUrl.length === 0 || currentUrl === feeds[i]['link']) {
                for (var j = 0; j < feeds[i]['posts'].length; j++) {
                    listModelPost.append(feeds[i]['posts'][j])
                }
            }
        }

        // scroll to top
        listPosts.scrollToTop()

        // scroll bottom bar
        if (currentUrl.length === 0) {
            listFeeds.contentX = -Theme.itemSizeMedium - Theme.paddingMedium
        }
    }

    allowedOrientations: Orientation.All

    onStatusChanged: {
        if (status == PageStatus.Active && isUpdate) {
            root.update(true)
        }
    }

    ListModel {
        id: listModelPost
    }

    ListModel {
        id: listModelFeed
    }

    SilicaListView {
        id: listPosts

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: bottomBar.top
            bottomMargin: Theme.horizontalPageMargin
        }

        visible: !loading
        spacing: Theme.paddingMedium
        model: listModelPost
        delegate: DelegatePost {
            onClicked: Qt.openUrlExternally(link)
        }
        header: PageHeader {
            title: qsTr("Posts")
        }

        PullDownMenu {
            id: menu

            topMargin: Theme.paddingLarge
            bottomMargin: Theme.paddingLarge

            MenuItem {
                text: qsTr("Edit feeds")

                onClicked: pageStack.push(Qt.resolvedUrl("PageFeeds.qml"), {feeds: feeds, setIsUpdate: function() {
                    isUpdate = true
                }})
            }

            MenuItem {
                text: qsTr("Reload")

                onClicked: root.update(true)
            }
        }

         VerticalScrollDecorator {}
    }

    SilicaListView {
        id: listSkeleton

        anchors {
            fill: parent
            bottomMargin: Theme.horizontalPageMargin
        }

        visible: loading
        spacing: Theme.paddingLarge
        interactive: false
        model: ListModel {
            ListElement { isTime: true }
            ListElement { isTime: true }
            ListElement { isTime: true }
            ListElement { isTime: true }
            ListElement { isTime: true }
            ListElement { isTime: true }
            ListElement { isTime: true }
        }
        delegate: DelegateSkeleton {}
        header: PageHeader {
            title: qsTr("Posts")
        }

        PullDownMenu {
            busy: true
        }
    }

    Item {
        id: bottomBar

        anchors.bottom: parent.bottom
        width: parent.width
        height: Theme.itemSizeExtraLarge
        visible: !loading

        Rectangle {
            anchors.fill: parent
            color: Theme._coverOverlayColor
        }

        SilicaListView {
            id: listFeeds

            anchors {
                left: parent.left
                right: parent.right
                verticalCenter: parent.verticalCenter
                margins: Theme.horizontalPageMargin
            }
            height: Theme.itemSizeMedium
            orientation: ListView.Horizontal
            spacing: Theme.paddingMedium
            model: listModelFeed
            quickScroll: true
            header: Item {
                width: parent.height + Theme.paddingMedium
                height: parent.height

                BottomBarItemAll {
                    isCurrent: root.currentRSS.length === 0

                    onClicked: updateLists("", feeds)
                }
            }
            delegate: BottomBarItemFeed {
                isCurrent: root.currentRSS === model.link

                onClicked: updateLists(model.link, feeds)
            }

            VerticalScrollDecorator {}
        }
    }

    Component.onCompleted: {
        root.update(false)
    }
}
