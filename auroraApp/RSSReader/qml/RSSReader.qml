/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow {
    function toastShow(text) {
        Notices.show(
                    text,
                    Notice.Short,
                    Notice.Bottom,
                    0,
                    -Theme.horizontalPageMargin)
    }

    objectName: "applicationWindow"
    initialPage: Qt.resolvedUrl("pages/PageSplash.qml")
    cover: Qt.resolvedUrl("cover/DefaultCoverPage.qml")
    allowedOrientations: Orientation.All

    // init webview with KMM js
    KMMAgent {
        id: agent
        onCompleted: {
            pageStack.animatorReplace(Qt.resolvedUrl("pages/PageMain.qml"), {}, PageStackAction.Replace)
        }
    }
}
