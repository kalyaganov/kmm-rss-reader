/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import {JsRssReader} from "./Constants";
import {Helper} from "./Helper";

// mappers
export * from './mappers/FeedMapper';

export const Service = {
    get: {
        getAllFeeds: function (forceUpdate = true) {
            return Helper.request(async () => {
                return await JsRssReader.getAllFeedsPromise(forceUpdate)
            }, (response) => {
                return response.toArray().mapToFeeds();
            }, 0)
        },
        addFeed: function (url) {
            return Helper.request(async () => {
                return await JsRssReader.addFeedPromise(url)
            }, (response) => {
                return response
            }, 0)
        },
        deleteFeed: function (url) {
            return Helper.request(async () => {
                return await JsRssReader.deleteFeedPromise(url)
            }, (response) => {
                return response
            }, 0)
        },
        loadFeed: function (url) {
            return Helper.request(async () => {
                return await JsRssReader.loadFeedPromise(url)
            }, (response) => {
                return response.mapToFeed();
            }, 0)
        },
    }
}
