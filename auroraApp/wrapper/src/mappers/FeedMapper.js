/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
/**
 * Map class KMM to object
 */
import shared from "shared";

// mappers
export * from './PostMapper';

/**
 * Export class from module
 */
export const Feed = shared.com.github.jetbrains.rssreader.core.entity.Feed

/**
 * Map [Feed] to object
 */
Feed.prototype.mapToFeed = function () {
    return {
        type: 'feed',
        title: this.title,
        link: this.link,
        desc: this.desc,
        imageUrl: this.imageUrl,
        sourceUrl: this.sourceUrl,
        isDefault: this.isDefault,
        posts: this.posts.toArray().mapToPosts(),
    }
};

/**
 * Ma [Feed] array to objects array
 */
Array.prototype.mapToFeeds = function () {
    return this.map((it) => it.mapToFeed())
};
