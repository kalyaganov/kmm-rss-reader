/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
/**
 * Map class KMM to object
 */
import shared from "shared";

/**
 * Export class from module
 */
export const Post = shared.com.github.jetbrains.rssreader.core.entity.Post

/**
 * Map [Post] to object
 */
Post.prototype.mapToPost = function () {
    return {
        type: 'post',
        title: this.title,
        link: this.link,
        desc: this.desc,
        imageUrl: this.imageUrl,
        date: this.date['r_1'],
    }
};

/**
 * Ma [Post] array to objects array
 */
Array.prototype.mapToPosts = function () {
    return this.map((it) => it.mapToPost())
};
